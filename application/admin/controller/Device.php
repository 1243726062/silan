<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;

use app\common;

class Device extends Controller
{
	/**
	 * 设备添加
	 */
	public function add()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		return view('add');
	}

	/**
	 * 设备列表
	 */
	public function index()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		//读取数据
		// $devices = db('device')->order('id asc')->paginate(10)->each(function($item, $key){
		// 	$list = explode("\n", $item['phone']);
		//     $item['phone_number'] = count($list)-1;
		//     return $item;
		// });
		$devices = db('device')->order('id asc')->paginate(10);

		// var_dump($devices);die;
		//显示模板
		return view('index', ['devices'=>$devices]);
	}

	/**
	 * 数据的添加
	 */
	public function insert()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		//获取表单中的post数据
		$data = request()->post();

		//数据入库
		if(db('device')->insert($data)){
			$this->success('添加成功','/devices');
		}else{
			$this->error('添加失败!!!');
		}
	}

	/**
	 * 设备的修改
	 */
	public function edit()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		//1. 读取当前要修改设备的基本信息
		$devices = db('device')->find(request()->get('id'));
		//2. 解析模板
		return view('edit', ['devices'=>$devices]);
	}

	/**
	 * 设备的更新
	 */
	public function update()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		//获取参数
		$data = request()->post();
	   	//更新数据库
	   	if(db('device')->update($data)){
	   		$this->success('更新成功','/devices');
	   	}else{
	   		$this->error('更新失败!!!');
	   	}
	}

	/**
	 * 删除设备
	 */
	public function delete()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		$id = request()->get('id');
		//取出设备名称
		$device_name = db('device')->where('id', $id)->value('device_name');
		// db('phone_card')->where('device_name', $device_name)->update($data);
		$data = db('phone_card')->where('device_name', $device_name)->select();
		
		$l=count($data);
		for($i=0;$i<$l;$i++){
			$data[$i]['device_name']="";
		}

		foreach($data as $item){
			db('phone_card')->update($item);
		}	
		//删除设备
		if(db('device')->delete($id)) {	
	   		$this->success('删除成功','/devices');
		}else{
	   		$this->error('删除失败');
		}
	}

    public function start(){
	    if(!is_session()){
	        $this->success('请登录！！！','/users/login');
	    }
		$device = db('device')->find(request()->get('id'));
		$value = request()->get('value');
		
		if($value == '1'){
			$this->error('已启用');
		}

		$device['status'] = 1;

		if(db('device')->update($device)){
			$this->success('启用成功', '/devices');
		}else{
			$this->error('启用失败');
		}
    }

    //设备禁用
    public function stop(){
        if(!is_session()){
            $this->success('请登录！！！','/users/login');
        }
    	$device = db('device')->find(request()->get('id'));
    	$value = request()->get('value');
    	
    	if($value == '0'){
    		$this->error('已禁用');
    	}

    	$device['status'] = 0;

    	if(db('device')->update($device)){
    		$this->success('禁用成功', '/devices');
    	}else{
    		$this->error('禁用失败');
    	}
    }


    public function search(){
    	$search = request()->post('search');
    	if($search){
            $devices = db('device')->where('device_name like \'%'.$search.'%\'')->paginate(10, false, ['query'=>['search' => $search]]);
        }else{
            $devices = db('device')->paginate(10);
        }
        return view('index', ['devices' => $devices]);
    }
}