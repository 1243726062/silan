<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;

use app\common;

class Phone extends Controller
{
	/**
	 * 手机号添加
	 */
	public function add()
	{
		$device = db('device')->order('id asc')->select();
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		return view('add',['device'=>$device]);
	}

	/**
	 * 手机号列表
	 */
	public function index()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		//读取数据
		$phones = db('phone_card')->order('id asc')->paginate(10);
		// var_dump($devices);die;

		//显示模板
		return view('index', ['phones'=>$phones]);
	}

	/**
	 * 数据的添加
	 */
	public function insert()
	{	
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		//获取表单中的post数据
		$data = request()->post();
		//数据处理
		//1. 添加状态字段	
		$device_name = $data['device_name'];

		$max_port = db('device')->where('device_name', $device_name)->value('port');
		if($data['device_port']>$max_port){
			$this->error("超过端口最大上限(最大上限为$max_port)");
		}


		$phone = db('device')->where('device_name', $device_name)->value('phone');
		$phone = $phone.$data['phone']."\n";

		$phone_number = db('device')->where('device_name', $device_name)->value('phone_number');
		$phone_number += 1;
		db('device')->where('device_name', $device_name)->update(['phone_number' => $phone_number]);

		if(db('device')->where('device_name', $device_name)->update(['phone' => $phone])){
			//数据入库
			if(db('phone_card')->insert($data)){
				$this->success('添加成功','/phones');
			}else{
				$this->error('添加失败!!!');
			}
		}
		
	}

	/**
	 * 手机号的修改
	 */
	public function edit()
	{	
		$device = db('device')->order('id asc')->select();
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		//1. 读取当前要修改手机号的基本信息
		$phones = db('phone_card')->find(request()->get('id'));
		//2. 解析模板
		return view('edit', ['phones'=>$phones, 'device' => $device]);
	}

	/**
	 * 手机号的更新
	 */
	public function update()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		//获取参数
		$data = request()->post();

		//修改设备表
		if($data['old_device'] != $data['device_name']){
			$old_device = db('device')->where('device_name', $data['old_device'])->find();
			$device = db('device')->where('device_name', $data['device_name'])->find();

			$old_device['phone'] = str_replace($data['phone']."\n", '', $old_device['phone']);
			$device['phone'] = $device['phone'].$data['phone']."\n";

			$old_device['phone_number'] = $old_device['phone_number'] - 1;
			$device['phone_number'] = $device['phone_number'] + 1;

			db('device')->update($device);
			db('device')->update($old_device);
		}
		unset($data['old_device']);

	   	//更新数据库
	   	if(db('phone_card')->update($data)){
	   		$this->success('更新成功','/phones');
	   	}else{
	   		$this->error('更新失败!!!');
	   	}
	}

	/**
	 * 删除手机号
	 */
	public function delete()
	{
		if(!is_session()){
			$this->success('请登录！！！','/users/login');
		}
		$id = request()->get('id');

		$phone = db('phone_card')->where('id',$id)->value('phone');

		// 修改设备表
		$device_name = db('phone_card')->where('id',$id)->value('device_name');
		if($device_name){
			$device = db('device')->where('device_name', $device_name)->find();
			$device['phone_number'] = $device['phone_number'] - 1;
			$device['phone'] = str_replace($phone."\n", '', $device['phone']);
			db('device')->update($device);
		}
			

		//删除手机号
		if(db('phone_card')->delete($id)) {
	   		$this->success('删除成功','/phones');
		}else{
	   		$this->error('删除失败');
		}
	}

    public function start(){
	    if(!is_session()){
	        $this->success('请登录！！！','/users/login');
	    }
		$phone = db('phone_card')->find(request()->get('id'));
		$value = request()->get('value');
		
		if($value == '1'){
			$this->error('已启用');
		}

		$phone['status'] = 1;

		if(db('phone_card')->update($phone)){
			$this->success('启用成功', '/phones');
		}else{
			$this->error('启用失败');
		}
    }

    //手机号禁用
    public function stop(){
        if(!is_session()){
            $this->success('请登录！！！','/users/login');
        }
    	$phone = db('phone_card')->find(request()->get('id'));
    	$value = request()->get('value');
    	
    	if($value == '0'){
    		$this->error('已禁用');
    	}

    	$phone['status'] = 0;

    	if(db('phone_card')->update($phone)){
    		$this->success('禁用成功', '/phones');
    	}else{
    		$this->error('禁用失败');
    	}
    }

    public function search(){
    	$search = request()->post('search');
    	if($search){
            $phones = db('phone_card')->order('device_port asc')->where('device_name like \'%'.$search.'%\' OR phone like \'%'.$search.'%\' OR user like \'%'.$search.'%\'')->paginate(10, false, ['query'=>['search' => $search]]);
        }else{
            $phones = db('phone_card')->order('id asc')->paginate(10);
        }
        return view('index', ['phones' => $phones]);
    }
}