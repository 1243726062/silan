<?php
namespace app\admin\controller;
use think\Controller;


class Users extends Controller
{   
    public function session()
    {
        session_start();
        if(empty($_SESSION)){
            $this->success('请登录','/admin');
        }else if($_SESSION['username']!='admin'){
                $this->success('请登录','/admin');
        }
    }

	public function index()
	{  
        $this->session();
		//读取数据
		$users = db('users')->order('id desc')->paginate(10);
		//显示模板
		return view('index', ['users'=>$users]);
	}

	public function add(){
        $this->session();
		return view('add');
	}

	public function insert(){
        if(!is_session()){
            $this->success('请登录！！！','/users/login');
        }		
		//获取表单中的post数据
		$data = request()->post();
		if($data['password'] != $data['repassword']){
			$this->error('密码输入不一致！！');
		}

		$data['password'] = md5($data['password']);
		
		//销毁确认密码的值
		unset($data['repassword']);	
		
		//填充数据
		// $data['username'] = generateRandomString();
		// $data['password'] = md5('害虫');
		// $data['picture'] = '/uploads/20180603/2bc68fec8143008baa234f8955e42989.jpg';


		$file = request()->file('profile');

		if($file){
	        $path = ROOT_PATH . 'public' . DS . 'uploads';
	        $info = $file->move($path);

	        if($info){
	            $filePath = '/uploads/' . $info->getSaveName(); 
	            $filePath = str_replace('\\', '/', $filePath);

	            $data['profile'] = $filePath;
	        }else{
	            // 上传失败获取错误信息
	            echo $file->getError();
	        }
    	}

    	$data['addtime'] = date('Y-m-d H:i:s');

    	if(db('users')->insert($data)){
    		$this->success('添加成功！！', '/users');
    	}else{
    		$this->error('添加失败!!');
		}
    }

    //用户修改
    public function edit(){
        $this->session();
    	$user = db('users')->find(request()->get('id'));

    	return view('edit', ['user'=>$user]);
    }

    //用户删除
    public function delete(){
        if(!is_session()){
            $this->success('请登录！！！','/users/login');
        }
    	$id = request()->get('id');

    	if(db('users')->delete($id)){
    		$this->success('删除成功', '/users');
    	}else{
    		$this->error('删除失败');
    	}
    }

    //用户启用
    public function start(){
        if(!is_session()){
            $this->success('请登录！！！','/users/login');
        }
    	$user = db('users')->find(request()->get('id'));
    	$value = request()->get('value');
    	
    	if($value == '1'){
    		$this->error('已启用');
    	}

    	$user['status'] = 1;
  
    	if(db('users')->update($user)){
    		$this->success('启用成功', '/users');
    	}else{
    		$this->error('启用失败');
    	}
    }

    //用户禁用
    public function stop(){
        if(!is_session()){
            $this->success('请登录！！！','/users/login');
        }
    	$user = db('users')->find(request()->get('id'));
    	$value = request()->get('value');
    	
    	if($value == '0'){
    		$this->error('已禁用');
    	}

    	$user['status'] = 0;

    	if(db('users')->update($user)){
    		$this->success('禁用成功', '/users');
    	}else{
    		$this->error('禁用失败');
    	}
    }

    public function update(){
        if(!is_session()){
            $this->success('请登录！！！','/users/login');
        }
    	$data = request()->only(['id','username','password']);

    	$data['password'] = md5($data['password']);

    	$file = request()->file('profile');

		if($file){
	        $path = ROOT_PATH . 'public' . DS . 'uploads';
	        $info = $file->move($path);

	        if($info){
	            $filePath = '/uploads/' . $info->getSaveName(); 
	            $filePath = str_replace('\\', '/', $filePath);

	            $data['profile'] = $filePath;
	        }else{
	            // 上传失败获取错误信息
	            echo $file->getError();
	        }
    	}

    	if(db('users')->update($data)){
    		$this->success('更新成功', '/users');
    	}else{
    		$this->error('更新失败');
    	}
    }

    public function login()
    {
        return view('login');
    }

    public function check_login()
    {
        $data = request()->post();
        $user = ['username'=>'admin','password'=>md5('admin')];
        if($user['username']==$data['username']){
            if(md5($data['password'])==$user['password']){
                session_start();
                $_SESSION['username'] = $data['username'];
                $this->success('登陆成功!!!','/devices');
            }else{
                $this->error('登录失败!!!!');
            }
        }else{
            $this->error('登录失败!!!!');
        }
    }

    public function esc()
    {
        session_start();
        $_SESSION = [];
        $this->success('退出完毕！！！','/admin');
    }
}