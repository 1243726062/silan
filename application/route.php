<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

//页面管理
// Route::rule('','home/Home/index');
// Route::rule('/home/mv_detail','home/Home/detail');
// Route::rule('/home/mv_list','home/Home/mv_list');
// Route::rule('/home/search','home/Home/search');
// Route::rule('/home/register','home/Home/show_register');
// Route::rule('/register','home/Home/register');
// Route::rule('/home/login','home/Home/show_login');
// Route::rule('/userlogin','home/Home/userlogin');
// Route::rule('/home/esc','home/Home/esc');

@
//设备管理
Route::rule('/devices','admin/Device/index');
Route::rule('/devices/edit','admin/Device/edit');
Route::rule('/devices/delete','admin/Device/delete');
Route::rule('/devices/update','admin/Device/update');
Route::rule('/devices/add','admin/Device/add');
Route::rule('/devices/insert','admin/Device/insert');
Route::rule('/devices/start','admin/Device/start');
Route::rule('/devices/stop','admin/Device/stop');
Route::rule('/devices/search','admin/Device/search');

//手机管理
Route::rule('/phones','admin/Phone/index');
Route::rule('/phones/edit','admin/Phone/edit');
Route::rule('/phones/delete','admin/Phone/delete');
Route::rule('/phones/update','admin/Phone/update');
Route::rule('/phones/add','admin/Phone/add');
Route::rule('/phones/insert','admin/Phone/insert');
Route::rule('/phones/start','admin/Phone/start');
Route::rule('/phones/stop','admin/Phone/stop');
Route::rule('/phones/search','admin/Phone/search');



//管理员登录
Route::rule('','admin/Users/login');
Route::rule('/admin/check_login','admin/Users/check_login');
Route::rule('/admin/esc','admin/Users/esc');

//视频管理
// Route::rule('/videos','admin/Video/index');
// Route::rule('/videos/edit','admin/Video/edit');
// Route::rule('/videos/delete','admin/Video/delete');
// Route::rule('/videos/update','admin/Video/update');
// Route::rule('/videos/add','admin/Video/add');
// Route::rule('/videos/insert','admin/Video/insert');
// Route::rule('/videos/start','admin/Video/start');
// Route::rule('/videos/stop','admin/Video/stop');
