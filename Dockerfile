FROM tutum/lamp:latest
COPY . /app
COPY mysql-setup.sh /
ENV MYSQL_ALLOW_EMPTY_PASSWORD yes
ENV APACHE_CONF_FILE_NAME apache2.conf
ENV WORK_PATH /etc/apache2
ENV OLD_DOCUMENTROOT /var/www/html
ENV NEW_DOCUMENTROOT /var/www/html/public
ENV DEFAULT_FILE sites-enabled/000-default.conf
ENV NEW_DEFAULT_FILE sites-available/000-default.conf

RUN sed -i --follow-symlinks '4s:/var/www/html:/var/www/html/public:g' $WORK_PATH/$DEFAULT_FILE \
&& sed -i '155s/AllowOverride None/AllowOverride All/' $WORK_PATH/$APACHE_CONF_FILE_NAME \
&& sed -i '166s/AllowOverride None/AllowOverride All/' $WORK_PATH/$APACHE_CONF_FILE_NAME \
&& sed -i --follow-symlinks '5,23d' $WORK_PATH/$DEFAULT_FILE

EXPOSE 80 3306
CMD ["/run.sh"]
