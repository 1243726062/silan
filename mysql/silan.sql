/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : silan

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2018-07-13 10:26:42
*/

create database `silan` default character set utf8 collate utf8_general_ci;
use silan;

SET FOREIGN_KEY_CHECKS=0;
SET character_set_database = utf8;
SET character_set_server = utf8;
SET character_set_client = utf8;
SET character_set_connection = utf8;
SET character_set_results = utf8;


-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS `device`;
CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(255) DEFAULT NULL,
  `device_ip` varchar(255) DEFAULT NULL,
  `phone_number` int(50) DEFAULT '0',
  `phone` varchar(10000) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `port` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device
-- ----------------------------
INSERT INTO `device` VALUES ('43', '4号设备', '192.168.0.109', '3', '18652960608\n13474817706\n', '1', '16');

-- ----------------------------
-- Table structure for phone_card
-- ----------------------------
DROP TABLE IF EXISTS `phone_card`;
CREATE TABLE `phone_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_port` int(20) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phone_card
-- ----------------------------
INSERT INTO `phone_card` VALUES ('19', '13474817706', '4号设备', '3', '', '1');
INSERT INTO `phone_card` VALUES ('18', '18652960608', '4号设备', '2', '', '1');

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES ('0', 'PBL');
INSERT INTO `types` VALUES ('1', 'TinsTa');
